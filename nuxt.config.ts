// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  alias: {
    '@': __dirname,
    '@svgs': __dirname.concat('/assets/svgs'),
  },
})
